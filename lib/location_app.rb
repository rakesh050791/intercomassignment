require 'data_manager'
require 'computation'
require 'json'

class LocationApp
  class << self
    def call(customers)
      compute = Computation.new #Initialize Compute object to process the data
      DataManager.manage_user_data(parse_json(customers), compute) # Processing Customers JSON file
      compute.result
      compute.result_array
    end

    private
    def parse_json data
      JSON.parse(data) # Private Method to parse json 
    end
  end
end