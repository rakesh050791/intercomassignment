class DataManager 
  class << self
    def manage_user_data(cust_json, compute)
      cust_json.each do |input_cust_details|
        compute.set_location(input_cust_details)
      end
    end
  end
end