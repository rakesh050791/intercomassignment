require 'user_detail'
require 'compass'

class Computation 

  module INTERCOM
    LATITUDE = 53.339428
    LONGITUDE = -6.257664
  end

  def initialize
    @users_hash = {}
    @distance_calulations = []
    @result_array = []
  end

  def set_location cust_details
    user = UserDetail.new
    user.user_id = cust_details["user_id"]
    user.distance = Compass.calulate_distance(INTERCOM::LATITUDE,INTERCOM::LONGITUDE,cust_details["latitude"].to_f,cust_details["longitude"].to_f)
    if user.distance < 100
      @users_hash = user
      @distance_calulations << @users_hash
    end
  end

  def result
    @distance_calulations.each do |user|
      @result_array << UserDetail.new(user.user_id,user.distance).call #if user.distance < 100
    end
  end
  
  attr_accessor :users_hash,:distance_calulations,:result_array
end

