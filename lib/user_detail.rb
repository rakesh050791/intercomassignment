class UserDetail
  
  def initialize(*args)
    @user_id = args[0]
    @distance = args[1]
  end

  def call
    {
      user_id: user_id,
      distance: distance
    }
  end
  
  attr_accessor :user_id, :distance
end