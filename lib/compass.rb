module Compass
  EARTH_RADIUS_KM = 6371.00

  def self.calulate_distance(lat_a, lng_a, lat_b, lng_b)
    return self.distance_in_radius(lat_a, lng_a, lat_b, lng_b) * EARTH_RADIUS_KM
  end

  private

  def self.distance_in_radius(lat_a, lng_a, lat_b, lng_b)
    d_lat = (lat_a - lat_b).to_rad;
    d_lng = (lng_a - lng_b).to_rad;

    a = Math.sin(d_lat/2)**2 + Math.cos(lat_a.to_rad) * Math.cos(lat_b.to_rad) * Math.sin(d_lng/2) ** 2
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

    return c
  end
end

class Float
  RADIAN_PER_DEGREE = Math::PI / 180.0

  def to_rad
    return self * RADIAN_PER_DEGREE
  end
end

class Integer
  RADIAN_PER_DEGREE = Math::PI / 180.0

  def to_rad
    return self * RADIAN_PER_DEGREE
  end
end
