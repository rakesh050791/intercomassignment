require 'location_app'
require 'json'

RSpec.describe "integration" do
  let(:customers_json) {
    <<-JSON
    [
      {"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"},
      {"latitude": "53.807778", "user_id": 28, "name": "Charlie Halligan", "longitude": "-7.714444"},
      {"latitude": "54.133333", "user_id": 24, "name": "Rose Enright", "longitude": "-6.433333"},
      {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
    ]
    JSON
  }

  let(:expected_result_json) {
    [
      {"user_id"=>12, "distance"=>41.7687255008362}, 
      {"user_id"=>24, "distance"=>89.03103382223571}
    ]
  }

  describe LocationApp do
    subject(:result) do
      json_result = LocationApp.call(customers_json)
      JSON.load(json_result.to_json)
    end

    it "outputs JSON in expected form" do
      expect(result).to eq JSON.load(expected_result_json.to_json)
    end
  end

end
